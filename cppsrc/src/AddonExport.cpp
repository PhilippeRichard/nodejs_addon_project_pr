#include "AddonExport.h"

#include <algorithm>
#include <chrono>
#include <cstring>
#include <iostream>
#include <mutex>
#include <thread>

using v8::Exception;
using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Value;
using v8::Uint32;
using v8::Map;

/*To start thread to pool value


*/


static std::mutex theLock;

void poolingTask(void) {
	for (int i = 1; i <= 5; ++i) {
		{
			std::lock_guard<std::mutex> lock(theLock);
			printf("\n%d",i);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	
	}
}

void StartPoolingTask(const FunctionCallbackInfo<Value>& args) {
	Isolate* isolate = args.GetIsolate();
	std::thread t(poolingTask);
	args.GetReturnValue().Set(String::NewFromUtf8(
		isolate, "Started thread Success"));

}



typedef struct string_array{
	char **arr;
	size_t size;
} str_array;


void free_str_array(str_array &array) {
	for (int i = 0; i < array.size; i++)
		free(array.arr[i]);
	free(array.arr);
	array.size = 0;
	array.arr = NULL;
}

const char* toChar(const String::Utf8Value& value) {
	return *value ? *value : "<string conversion failed>";
}

std::string toCString(const String::Utf8Value& value) {
	return *value ? *value : "<string conversion failed>";
}