{
  "targets": [
    {
      "target_name": "AddonExport",
      "defines": ["PRINT_ENABLED=1"],
      "cflags_cc": ["-std=c++11"],
      'xcode_settings': {
        'CLANG_CXX_LANGUAGE_STANDARD': 'c++11',
      },
      "sources": [
		"cppsrc/src/AdoonExport.cpp",	
		
      ],
      "include_dirs": [
		"cppsrc/include"
		
      ],
      "conditions": [
      
      ],
	
    
    	 'defines': [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ]
    },
  ]
}